CMake is the build tool for CDaq.
Download it from http://www.cmake.org/cmake/resources/software.html

1) Install
2) Run
3) Use root-directory of the whole packages as "Where is the source code:"
   directory. (Root dir containst the first CMakeLists.txt)
4) Use <root>/build as "Where to build the binaries"
5) Click "Configure"
   - Choose "Visual Studio 10 Win64" for 64-bit build
   - Choose "Visual Studio 10" for 32-bit build
   - Check "Use default native compilers"
6) Click "Generate"
7) Open <root>/build/cdaq.sln with Visual Studio
