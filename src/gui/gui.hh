#ifndef CDAQ_GUI_GUI_HH
#define CDAQ_GUI_GUI_HH

#include <QMainWindow>
#include <QListWidget>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include "cdaq/daq/daq.hh"
#include "cdaq/camera/camera.hh"
#include <boost/array.hpp>
#include "cdaq/daq/daq.hh"
#include "cdaq/daq/data_to_csv.hh"
#include "cdaq/misc/date.hh"
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <boost/thread.hpp>
#include <QPushButton>
#include <QLineEdit>

namespace cdaq {

/// \class CdaqGui
/// \brief Ugly GUI for displaying image from a single camera and data from
///     a single daq.
class CdaqGui
    :
    public QMainWindow
{
    Q_OBJECT
 public:
    /// Construct CdaqGui
    /// \param[in] port Serial port which is used to communicate with 
    ///     daq
    /// \param[in] hz Sampling rate which will be requested from the daq
    /// \param[in] cam_num Camera identification number. 0 based indexing
    /// \param[in] width Width of the captured image which will be requested
    ///     from the camera.
    /// \param[in] height Height of the captured image which will be requested
    ///     from the camera.
    CdaqGui(std::string &port = std::string("com3"), const int &hz = 1000, 
        const unsigned int &cam_num = 0, 
        const unsigned int &width = 640, const unsigned int &height = 480);
    ~CdaqGui();    
    
 private slots:
    /// Get and process daq data
    void GetDaqData();
    
    /// Draw image to the screen
    /// \param[in] img Image to be drawn on the screen
    void DrawImage(Image img);
    
    /// Enable/disable saving of data
    /// \param[in] capture If true, captured data will be saved
    /// \warning Previous daq data in the csv file will be overwritten
    ///     the filename is not changed
    void ToggleSaving(bool capture);
 private:
    /// Initialize plot curve/channel list
    void SetPlotList();
    
    /// Function in which a thread will acquire images from the camera
    void CameraCaptureLoop();
    
    /// Start capturing images from the camera
    void StartCameraCapturing();
    
    /// Stop capturing images from the camera
    void StopCameraCapturing();
 private:
    // Date when the capturing was started. Used for scaling/labeling x-axis
    Date record_start_date_;
    
    // Daq/camera which are used for data capturing
    Daq daq_;
    Camera camera_;
    
    // Object to which the data from the daq will be fed when saving
    // the data
    DataToCsv daq_saver_;
    
    // Plot surface (compare to MATLABs "axes")
    QwtPlot *plot_;
    
    // Curves (compare to MATLABs "line")
    boost::array<QwtPlotCurve*, Daq::kNumberOfChannels> curves_;
    
    // Vertical widget with channel/curve names
    QListWidget *plot_list_;
    
    // Last drawn image
    QGraphicsPixmapItem *image_;
    
    // "Space"/Scene where the image is located
    QGraphicsScene *scene_;
    
    // Surface to which the "Space"/Scene can be drawn
    QGraphicsView *view_;
    
    // Button for starting/stopping capture/saving
    QPushButton *button_capture_;
    
    // Single line edit boxes for input data
    QLineEdit *edit_folder_;
    QLineEdit *edit_csv_filename_;
    QLineEdit *edit_tag_;
    
    // Thread which captures images from camera.
    // We do not use main thread as drawing of images/plotting curves
    // could cause us to miss one of the images/timestamps could be
    // wrong
    boost::thread thread_capture_;
    
    // If false, capturing thread will exit
    bool continue_capturing_;
    
    // If true, we can draw next image (will prevent capturing thread
    // from eating all of the memory by making calls to DrawImage which
    // can not be processed fast enough)
    bool image_needs_updating_;
    
    // Protects 'image_needs_updating_' 
    boost::mutex mutex_needs_updating_;
    
    // If true, daq data should be saved
    bool save_data_;
}; //class CdaqGui
    
} //namespace cdaq

#endif //ifndef CDAQ_GUI_GUI_HH
