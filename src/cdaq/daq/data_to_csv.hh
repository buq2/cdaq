#ifndef CDAQ_DAQ_DATA_TO_CSV_HH
#define CDAQ_DAQ_DATA_TO_CSV_HH

#include "cdaq/daq/daq.hh"
#include <string>
#include <iostream>
#include <fstream>

namespace cdaq {

/// \class DataToCsv
/// \brief Writes CSV data to disk
class CDAQDAQAPI DataToCsv
{
 public:
    /// Create CSV writing object which writes the input data to file
    /// \param[in] outfname To which file the data should be written
    DataToCsv(const std::string &outfname)
    {
        Open(outfname);
    }
    
    /// Create CSV writing object, but do not open a file
    DataToCsv()
    {
    }
    
    /// Open new file to which data should be written
    /// \param[in] outfname To which file the data should be written
    void Open(const std::string &outfname)
    {
        if (file_.is_open()) {
            Close();
        }
        file_.open(outfname);
    }
    
    /// Close file to which data was being written
    void Close()
    {
        file_.close();
    }
    
    ~DataToCsv()
    {
        Close();
    }
    
    /// Add data directly to the file
    template<typename T>
    void AddData(const T &data)
    {
        file_ << data;
        // Make sure data is written -> if program is forcibly closed
        // at least some of the data will remain
        // Slow, but, but...
        file_.flush(); 
    }
    
    /// Add boost::tuple type data to the file. Writes a single
    /// line as comma separated list
    template <typename SampleT>
    void AddData(const boost::tuple<Date, SampleT> &data)
    {
        AddData(data.get<0>().Microseconds());
        const SampleT sample = data.get<1>();
        for (unsigned int ii = 0; ii < sample.size(); ++ii) {
            AddData(",");
            AddData(sample[ii]);
        }
    }
    
    /// Add vector type of data to the file
    template <typename DatadSampleT>
    void AddData(const std::vector<DatadSampleT> &data) 
    {
        for (unsigned int ii = 0; ii < data.size(); ++ii) {
            AddData(data[ii]);
            AddData('\n');
        }
    }
 private:
    std::ofstream file_;
}; //class DataToCsv

} //namespace cdaq

#endif //ifndef CDAQ_DAQ_DATA_TO_CSV_HH
